Capo 1st Fret/ OR 4th


[Verse 1]

C
When I get older losing my hair,
                 G
Many years from now.

Will you still be sending me a Valentine.
                 C
Birthday greetings bottle of wine.


[Verse 2]

If I'd been out till quarter to three.
C7                   F
Would you lock the door.
F       Fm               C      A
Will you still need me, will you still feed me,
D       G7      C
When i'm sixty-four.


[Verse 3]

Am               E
You'll be older too,
Am              D7
And if I say the word,
F       G       C
I could stay with you.


[Verse 4]

C
I could be handy, mending a fuse
                         G
When your lights have gone.
G7
You can knit a sweater by the fireside
                C
Sunday morning go for a ride,


[Verse 5]

Doing the garden, digging the weeds,
C7              F
Who could ask for more.
F       Fm              C       A
Will you still need me, will you still feed me
D       G7      C
When I'm sixty-four.


[Verse 6]

Am
Every summer we can rent a cottage,
              G                 Am
In the Isle of Wight, if it's not too dear
                       E
We shall scrimp and save
Am               D7
Grandchildren on your knee
F      G                C
Vera Chuck and Dave