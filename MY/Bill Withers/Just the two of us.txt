Just The Two Of Us  Bill Withers and Grover Washington Jr.
Capo 1st fret for original key

[Verse]

Cmaj7     B7                Em7          Dm7    G7    Cmaj7           
I see the crystal raindrops fall and the beauty of it all 
            B7                Em      Em7 
is when the sun comes shining through
Cmaj7              B7             Em7         Dm7      G7       Cmaj7
     To make those rainbows in my mind when I think of you some time 
              B7                   Em   Em7
and I want to spend some time with you


[Chorus]

         Cmaj7     B7         Em7           Dm7  G7       Cmaj7 
Just the two of us     We can make it if we try. Just the two of us 
B7            Em        Em7
    (Just the Two of Us)
         Cmaj7     B7           Em7            Dm7  G7       Cmaj7
Just the two of us     building castles in the sky. Just the two of us,  
B7           Em   Em7
     You and I


[Verse]

Cmaj7             B7                Em7          Dm7     G7       Cmaj7
      We look for love, no time for tears wasted waters' all that is 
             B7              Em    Em7
and it don't make no flowers grow
Cmaj7                   B7                Em7         Dm7       G7       Cmaj7
      Good things might come to those who wait not to those who wait too late 
          B7            Em     Em7
we got to go for all we know


[Chorus]

         Cmaj7     B7         Em7           Dm7  G7       Cmaj7 
Just the two of us     We can make it if we try. Just the two of us 
B7            Em        Em7
    (Just the Two of Us)
         Cmaj7     B7                Em7            Dm7  G7       Cmaj7
Just the two of us     building them castles in the sky. Just the two of us,  
B7           Em   Em7
     You and I


[Solo] [[: Cmaj7  B7  A7sus4  A7  Bbmaj7  G7sus4  G7  Cmaj7  F13 :]] 2x
Back to this one time through: Cmaj7  B7 /  Em  Dm7  G7  /  Cmaj7  B7  / Em  Em7


[Verse]

Cmaj7            B7                Em7         Dm7    G7       Cmaj7
      I hear the crystal raindrops fall on the window down the hall 
         B7                Em     Em7
and it becomes the morning dew
Cmaj7          B7               Em7         Dm7     G7      Cmaj7         
      Darling, when the morning comes and I see the morning sun 
          B7              Em     Em7
I want to be the one with you


[Chorus]

         Cmaj7     B7         Em7           Dm7  G7       Cmaj7 
Just the two of us     We can make it if we try. Just the two of us 
B7            Em        Em7
    (Just the Two of Us)
         Cmaj7     B7               Em7     Dm7    G7            Cmaj7
Just the two of us     Building big castles way on high Just the two of us 
B7           Em     Em7
     you and I
          Cmaj7      B7       Em7               Dm7     G7       Cmaj7              
(Just the Two of Us) Just the two of us (We can make it just the two of us) 
     B7              Em   Em7
Lets get it together baby Yeah!
          Cmaj7            B7       Em7       
(Just the Two of Us) DEEP: Just the Two of Us 
        Dm7     G7       Cmaj7      B7     Em     Em7
(We can make it just the two of us)