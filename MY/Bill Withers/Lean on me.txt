 ...LEAN ON ME... by Bill Withers
---------------.................

*from 'Still Bill' (1972)*


[Intro]

C   Dm Em F    F Em Dm C   C Dm Em Dm
C   Dm Em F    F Em Dm C   C Dm Em G/B C


[Verse 1]

C          Dm Em  F
 Sometimes in our lives,
F   Em  Dm   C        Dm  Em       Dm
 We all have pain, we all have sor-row.
C        Dm Em  F
 But, if we are wise,
F   Em   Dm   C           Dm Em   G/B C
 We know that there's, al-ways to-mor-row.


[Chorus 1]

C                 Dm     Em  F
 Lean on me, when you're not strong,
    F    Em Dm   C            Dm   Em        Dm
And I'll be your friend; I'll help you carry on.
C       Dm    Em F           F   Em  Dm C
 For it won't be long, 'till I'm gon-na need,
     Dm   Em G/B  C
Some-body to lean on.


[Verse 2]

C             Dm  Em   F
 Please, swal-low your pride,
F   Em Dm   C           Dm   Em     Dm
 If I  have things, you need to bor-row.
C        Dm  Em  F     F     Em Dm   C
 For, no one can fill, those of your needs,
     Dm  Em    G/B C
That you don't let show.


[Bridge 1]

         N.C.
So, just call on me brother, when you need a hand;
N.C.
We all need somebody, to lean on.
N.C.
I just might have a problem, that you'd understand;
                         G/B  C
We all need somebody, to lean on.


[Chorus 2]

C                 Dm     Em  F
 Lean on me, when you're not strong,
    F    Em Dm   C            Dm   Em        Dm
And I'll be your friend; I'll help you carry on.
C       Dm    Em F           F   Em  Dm C
 For it won't be long, 'till I'm gon-na need,
     Dm   Em G/B  C
Some-body to lean on.


[Bridge 2]

         N.C.
So, just call on me brother, when you need a hand;
N.C.
We all need somebody, to lean on.
N.C.
I just might have a problem, that you'd understand;
                         G/B  C
We all need somebody, to lean on.


[Verse 3]

C          Dm Em F     F   Em   Dm C
 If, there is a  load, you have to bear,
     Dm  Em        Dm
That you can't car-ry.
C          Dm Em  F     F    Em    Dm   C
 I'm right up the road; I'll share your load,
   Dm  Em   G/B  C
If you just call me.


[Coda]

Dm    C                   Dm
 (Call me), if you need a friend,
Dm     C
 (Call me), call me,
Dm    C                   Dm
 (Call me), if you need a friend,
Dm     C
 (Call me), call me.
Dm     C
 (Call me), call me,
Dm     C
 (Call me), call me,
Dm     C
 (Call me), call me,
Dm     C
 (Call me), call me.
Dm     C
 (Call me), call me,
Dm     C
 (Call me), call me,
Dm     C
 (Call me).


CHORD DIAGRAMS:
---------------

   C       Dm      Em      F       G/B
 EADGBE  EADGBE  EADGBE  EADGBE  EADGBE
 x32010  xx0231  022000  133211  x20033

Tabbed by Joel from cLuMsY, Bristol, England, 2005 (clumsyband@hotmail.com)