Raz staruszek, spaceruj�c w lesie, 	e A7 e A7
Ujrza� listek przywi�d�y i blady 	e A7 H7
I pomy�la�: - Znowu idzie jesie�, 	e A7 e
Jesie� idzie, nie ma na to rady! 	C H7 e A7
I podrepta� do chaty po dr�ce, 	C D G e
I oznajmi�, stan�wszy przed chat� 	C D G e
Swojej �onie, tak samo staruszce 	C D G e
Jesie� idzie, nie ma rady na to! 	C H7 e A7 e H7

A staruszka zmartwi�a si� szczerze,
Zamacha�a r�kami obiema:
- Musisz zacz�� chodzi� w pulowerze.
Jesie� idzie, rady na to nie ma!
Mo�e zrobi� si� ch�odno ju� jutro
Lub pojutrze, a mo�e za tydzie�?
Trzeba b�dzie wyj�� z kufra futro,
Nie ma rady, jesie�, jesie� idzie!

A by� sierpie�. Pogoda prze�liczna.
Wszystko w z�ocie trwa�o i w zieleni.
Pr�cz staruszk�w nikt chyba nie my�la�
O maj�cej nast�pi� jesieni.
Ale c�, oni �yli najd�u�ej,
Mieli swoje staruszkowe zasady
I wiedzieli, �e wcze�niej czy p�niej -
Jesie� przyjdzie. Nie ma na to rady. 