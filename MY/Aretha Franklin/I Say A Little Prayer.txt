 There is no strumming pattern for this song yet. Add it and get +5 IQ

[Intro] Piano

F#m D E A  D C#7


[Verse]

F#m                D
The moment I wake up
             E        A
Before I put on my makeup
   D                      C#7
I say a little prayer for you
F#m              D 
While combing my hair, now,
                     E       A
And wondering what dress to wear, now,
   D                  C#7
I say a little prayer for you


[Chorus]

D           E            E          F#m
Forever and ever, you'll stay in my heart
            E
and I will love you
D           E            E        F#m
Forever, forever, we never will part
            E
Oh, how I'll love you
D           E             E        F#m
Together, together, that's how it must be
            E
To live without you
D            E            C#7
Would only be heartbreak for me.


[Verse]

F#m             D 
I run for the bus, dear,
                E       A
While riding I think of us, dear,
D                         C#7
I say a little prayer for you.
F#m            D
At work I just take time
                   E       A
And all through my coffee break-time,
   D                      C#7
I say a little prayer for you.


[Chorus]

D           E            E          F#m
Forever and ever, you'll stay in my heart
            E
and I will love you
D           E            E        F#m
Forever, forever, we never will part
            E
Oh, how I'll love you
D           E             E        F#m
Together, together, that's how it must be
            E
To live without you
D            E            C#7
Would only be heartbreak for me.


[End Bridge]
F#m            D
My darling believe me,
          E           
For me there is no one
A
But you.
A
Answer my Prayer
A
This is my prayer 