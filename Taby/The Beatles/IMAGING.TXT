#----------------------------------PLEASE NOTE---------------------------------#
#This file is the author's own work and represents their interpretation of the #
#song. You may only use this file for private study, scholarship, or research. #
#------------------------------------------------------------------------------##
From: rath@binah.cc.brandeis.edu

I was making room in my account by cleaning up some files today--
"Imagine was in chords w/ no words, words w/ no chords, and a correction 
to the chords with nothing else--here's all three combined and slightly 
road tested--something's missing in a few spots, but I don't have 
the record, time, or inclination to figure it out...it gets the main idea.
As I clean up, if I combine anymore of these, I'll post them.
	--Rich
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

IMAGINE--JOHN LENNON

C 	Cmaj7 	   F
Imagine there's no heaven
It's easy if you try
No hell below us
Above us only sky

F 	Am      Dmin7 F 
Imagine all the people
G      C/G   G7  
Living for today

C 	Cmaj7 	   F
Imagine there's no countries
It isn't hard to do
No greed or hunger
And no religion too

F 	Am      Dmin7 F 
Imagine all the people
G      C/G   G7  
Living life in peace


C - Cmaj7 - E - E7 
You may say I'm a dreamer
F 	G 	     C  E7
But I'm not the only one
F 	G 	       C     E7
I hope someday you'll join us
F 	G 		C    C
And the world will live as one

C 	Cmaj7 	   F
Imagine no possessions
I wonder if you can
Nothing to kill or die for
A brotherhood of man

F 	Am      Dmin7 F 
Imagine all the people
G      C/G   G7  
Sharing all the world


C - Cmaj7 - E - E7 
You may say I'm a dreamer
F 	G 	     C  E7
But I'm not the only one
F 	G 	       C     E7
I hope someday you'll join us
F 	G 		C    C
And the world will live as one


Date: Thu, 20 Jul 1995 10:54:29 +0200 (MET DST)
From: "Dipl.-Ing. Wolfgang Kruell" <kruell@sent5.uni-duisburg.de>

from:	kruell@sent5.uni-duisburg.de
	Wolfgang Kruell
------------------------------------------------------------------------

John Lennon :	Imagine

 G		    C	    G		     C
Imagine there's no heaven. It's easy if you try.
G	     C    G		 C  	 
No hell below us. Above us only sky.
C       e	A	D	    D7
Imagine all the people living for today.

D7  C	     D		G	H7 	
A-ha you may say I'm a dreamer.
C	D7	     G	  H7	
But I'm not the only one.
C	D		  G	H7
I hope some day you'll join us.
C	 D7	   G
And the world will be as one.

 G			C    G		       C
Imagine there's no countries, it isn't hard to do.
 G		     C       G		      C
Nothing to kill and die for, and no religion too.
C       e	A	D	    D7
Imagine all the people living for today.

D7  C	     D		G	H7 	
A-ha you may say I'm a dreamer.
C	D7	     G	  H7	
But I'm not the only one.
C	D		  G	H7
I hope some day you'll join us.
C	 D7	   G
And the world will be as one.

 G		   C	G		C
Imagine no possessions, I wonder if you can.
 G		      C	    G		      C 	
No need for greed or hunger, a brotherhood of man.
C       e	A	D	    D7
Imagine all the people living for today.

D7  C	     D		G	H7 	
A-ha you may say I'm a dreamer.
C	D7	     G	  H7	
But I'm not the only one.
C	D		  G	H7
I hope some day you'll join us.
C	 D7	   G
And the world will be as one.



Date: Fri, 24 Nov 95 23:49:45 CST
From: "Ben Weiss" <weis0010@gold.tc.umn.edu>
Subject: Re: REQ IMAGINE, JOHN LENNON


IMAGINE
Words & music: John Lennon

INTRO: C Cmaj7 C F 
(The Cmaj7 is a quick pull-off: C C C Cmaj7-C F F F F)

C               Cmaj7 C  F      C             Cmaj7 C F  
 Imagine there's      no heaven, it's easy if you     try
C        Cmaj7 C F      C         Cmaj7 C  F
 No hell       below us, above us on -  ly sky
[F]     C/E     Dm7   F/C G            C     G7
Imagine all the people    living for today...ahh

Imagine no more countries, it isn't hard to do
Nothing to kill or die for, and no religion too
Imagine all the people living life in peace...you-hoo...

        F        G         C       E7
CHORUS:  You may say I'm a dreamer
        F        G            C   E7
         But I'm not the only one
        F           G          C       E7
         I hope someday you'll join us
        F        G          C         Cmaj7 C F...
         And the world will be as one

Imagine no possessions, I wonder if you can
No need for greed or hunger, a brotherhood of man
Imagine all the people sharing all the world

CHORUS

OUTRO: Same as intro, end on C



