	Tapping on Bass Guitar Lesson #2
	================================
			Ian Stephenson
			ian@ohm.york.ac.uk


At the end of the last lesson you should have played the following  
riff from "All Along the WatchTower":

   e  e  e  e  e  e  e  e    e e  e  e  s s s s  s s s s
|--------14----------12----|------10--------10-------12----|
|o----14----14----12----12-|---10----10---10--10---12--12-o|
|o-12----------10----------|-8----------8--------10-------o|
|--------------------------|-------------------------------|


While that's all very pretty, we need to get a little more intense as  
the song progresses. This lesson looks at two variations of this  
riff.

The technique we can apply to this riff is "Double Stopping" -  
playing more than one note at once.

Previously we brought two fingers of the right hand down separately.  
Now bring them down in exactly the same way, but both together.

				         O
					---

					-O-

					---
			    __
|-----14----------|	|--/--\--------------------
|-----14----------|	|  \   | .
|-----------------|	|---@--|-------------------
|-----------------|	|      / .
      T			|-----/--------------------
			|    /
			|---/----------------------
			|
			|--------------------------
			
You should find this fairly easy, and should be able to play:	 
		

   q    q      e  e  q       q    q     e   e    e   e
|-------14-----------12----|------10--------10-------12----|
|o------14-----------12----|----------------10-------12---o|
|o-12----------10-10-------|-8----------8--------10-------o|
|--------------------------|-------------------------------|
   H3	T12    H1 H1 T12     H1   T12   H1  T12  H1  T12

Note I've marked an alternative left hand fingering here, using the  
third finger to fret the A at the 12th fret. This reduces the  
necessary hand movement, but you should make sure that you get the  
sound consistent between both fingers.

I've only notated the most basic of rhythms here.  Playing it straight  
will give the heaviest sound, but the riff may be looped indefinitely  
so try shifting the rhythm around.  You should be able to make the two  
hand parts "bounce off each other".

The notes we're playing are taken from the chords Am, G and F as  
follows

	         O
		---    	-O-
	                      	   O
		-O-    	---   	  ---
		       	 O
		-o-    	---       -O-
    __                   o          

|--/--\----------O---|--------|----o----||
|  \   | .	     |   O    |         ||
|---@--|-------------|--------|----O----||
|      / .           |        |         ||
|-----/--------------|--------|---------||
|    /               |        |         ||
|---/----------------|--------|---------||
|                    |        |         ||
|--------------------|--------|---------||
|                    |        |         ||
|                Am  |    G   |   F     ||
|                    |        |         ||
|                    |        |         ||
|----------------14--|---12---|---10----||
|----------------14--|---12---|---10----||
|----------------12--|---10---|---8-----||
|--------------------|--------|---------||

(The Major and minor chords are differentiated by the notes I've  
indicated as "o" (little "o") - we're not playing those yet. You may like to  
analyse how this works)

Double Stopping will help to thicken the sound, but the basic problem  
with this riff is that if you play this, then nobody is playing the  
bass part! That's fine for a while, but not exactly great for the big  
intense ending. 


The answer of course is to split your hands, and use the left hand to  
play the bass line one octave below where we've being playing it:

				         O
					---

					-O-

					---
			    __
|-----14----------|	|--/--\--------------------
|-----14----------|	|  \   | .
|-----------------|	|---@--|-------------------
|--5--------------|	|      / .
   H  T			|-----/--------------------
			|    /
			|---/----------------------
			|            O
			|--------------------------

That's pretty easy in itself, and gives a much fuller sound.  
Unfortunately you may find it a little harder when used in context.

   q    q      e  e  q       q    q     e   e    e   e
|-------14-----------12----|------10--------10-------12----|
|o------14-----------12----|----------------10-------12---o|
|o-------------------------|------------------------------o|
|--5-----------3-----------|-1----------1--------3---------|
   H2	T12    H1 H1 T12     H1   T12   H1  T12  H1  T12

This is will take some getting used to - particularly those of you  
who have been looking at the fretboard, as you can't watch both hands  
at once (I did warn you!). The only answer is familiarity with the  
bass - learning to feel your way round. Don't worry if you can't get  
this right now, but keep it in mind as something to practise,  
alongside future lessons.


The new chords may be written:


	         O
		---    	-O-
	                      	   O
		-O-    	---   	  ---
		       	 O
		-o-    	---       -O-
    __                   o          

|--/--\----------o---|--------|----o----||
|  \   | .	     |   o    |         ||
|---@--|-------------|--------|----o----||
|      / .       o   |        |         ||
|-----/--------------|---o----|---------||
|    /           o   |        |    o    ||
|---/----------------|---o----|---------||
|                O   |        |    o    ||
|--------------------|---O----|---------||
|                    |        |    O    ||
|                Am  |    G   |   F     ||
|                    |        |         ||
|                    |        |         ||
|----------------14--|---12---|---10----||
|----------------14--|---12---|---10----||
|--------------------|--------|---------||
|----------------5---|---3----|---1-----||
(Again I've marked notes that are implied as "o", played notes as  
"O"). Its pretty obvious why it sounds better.

Once you have mastered the splitting of your hands, you may like to  
try the first riff again with the bass line an octave down:

   e  e  e  e  e  e  e  e    e e  e  e  s s s s  s s s s
|--------14----------12----|------10--------10-------12----|
|o----14----14----12----12-|---10----10---10--10---12--12-o|
|o-------------------------|------------------------------o|
|--5-----------3-----------|-1----------1--------3---------|

Particular effort must be put into playing the F at the first fret of  
the E string - Hammering on requires a little more effort close to  
the nut, as there is less available movement from the string (it  
being anchored not very far away). You may have to hit the string  
just a little harder (or those with rapid detuning machanisms may  
take the string down a tone - Kubickis are great for this, as the nut  
is moved while the note remains in the same place).


While very basic, these riffs are in fact quite hard, and should be  
enough to keep you occupied for quite a while.

More Hendrix stuff next time...
Ian
------------------------------------------------------------------
Previous tapping and slapping lessons are available via FTP from
KAPPA.RICE.EDU in the directory /pub/bass/lessons.  If you do not
have FTP, and wish to retrieve the lessons via email, send mail to
ftpmail@decwrl.dec.com with the message "help".
------------------------------------------------------------------
