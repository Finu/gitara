Lessons in Bass Line Construction
Steve Schmidt
schmidsj@unvax.union.edu

Lesson 3 - Chords and Bass Lines

   In Lesson 1, we talked about using the root note of a chord to define the
chord, and in Lesson 2 we talked about using leading tones to move from
one chord root to the next. In this lesson, we'll talk about playing notes
from the chords other than the root notes, and about using them to construct
bass figures that you can play over several different chords.

   To repeat the definition from Lesson 1, a chord is any group of three or
more notes being played simultaneously. The simplest ones are groups of
exactly three notes: more complex ones are built by adding extra notes to one
of the basic ones. There are four three-note chords, but only two of them are
used in most forms of music, major chords and minor chords. (The other two,
augmented and diminshed chords, are used mostly in jazz and classical music. I
won't mention them again in this lesson, although they'll reappear in a later
lesson.)

   A major chord consists of three notes; the root note, a note which is two
whole steps above the root (called the third), and another note which is one
and one-half steps above the third (called the fifth). It may seem odd to call
the notes third and fifth instead of second and third; but there's a reason
for it, which I'll explain in the next lesson. To give an example, the three
note C, E, and G make up a C major chord. E, the third, is two whole steps
above the root note, C: and G, the fifth, is 1.5 steps above the E. If you
wanted to play these notes on your bass, you might finger them like this:

G------------
D------2--5-- 
A---3--------
E------------

and you'd get a C major chord. In fact, the pattern:

-----(N-1)--(N+2)-- 
--N----------------

  2   1      4

on any two consecutive strings will produce a major chord, and this is a
fingering that you can use over and over again in your bass lines. (The
numbers below the staff indicate fingerings: use your middle finger to play
the root, your index finger for the third, and your pinkie for the fifth. Then
you can reach all three notes without moving your left hand.)

   A minor chord is similar to a major chord, but the intervals are reversed:
that is, the third is 1.5 steps above the root, and the fifth is two steps
above the third. Thus, the notes C-Eb-G make up a C minor chord. Note that the
root and the fifth are the same: only the third differs, and that's what makes
the two chords sound different when played on a guitar. You can play a C minor
chord like this:

G------------
D---------5-- 
A---3--6-----
E------------

and in general the pattern:

------------(N+2)-- 
--N--(N+3)-----------

  1   4      3

produces the notes of a minor chord, and you can play all three without moving
your left hand.

In the past lessons we've used the root note of a chord to define it, but now
we have three notes of the chord that we can use to define it. We can play
just the root, as we've been doing, and that is sufficient; or we can play two
or three of them, if we like. Here's a bass line that does the latter: it's
the line from "Twist and Shout", which has been played by a lot of bands
including the Beatles. It also happens to be the bass line for "La Bamba" by
Ritchie Valens, by a strange twist of fate. Think of it as whichever one you
like.

   C major  F major      G major   F major

   q  e  e  e  e  e  e   e  q. e  e  e  e  e
G--------------2--5----|---------------------|
D-----2--5--3--------5-|-5--r--0--3--3--2--0-|   repeat
A--3------------------\_/--------------------|
E----------------------|---------------------|

  Cmon and shake it up baby (shake it up baby)
  Twist and Shout           (twist and shout)

You can see the outline of the C major chord in the first half-measure, just
as we wrote it above. You can also see the outline of the F major chord in the
second half of the first measure: it's the same pattern played one string
higher. For the G, we hit only one note, the root, and hold it: then we play
the root of the F chord, followed by a leading sequence back down to the C
major chord, where the phrase repeats.

You can also play two of the notes of the chord, rather than all three. The
bass line that is at the heart of almost all country music does that: it plays
the root and fifth on alternating beats.

(all notes are quarter notes)

   C major                   F major                    C major

G-------------|-------------|------------|-------5--r-|----
D-----r--5--r-|-----r--5--r-|-3--r-------|-3--r-------|----
A--3----------|--3----------|-------3--r-|------------|--3-
E-------------|-------------|------------|------------|----

It alternates root-fifth-root-fifth-root-fifth. Doesn't actually do much else,
but it does serve to outline the chord being played at all times. Because of
its simplicity and power, it's one of the most heavily used ideas for bass
lines in all of popular music; besides country music, bluegrass music, some
folk music, and occasional bits of rock and jazz use it as well. It does,
however, get boring after a while: you might like to use some leading notes to
jazz it up a little bit. One bass line that does so is the one from the song
"Wipeout" by the Beach Boys. It goes like this:

(all notes are 8th notes)

             E major

G----------|------------------------|------------------------|
D-----0--1-|-2--2--2--0--------0--1-|-2--2--2--0--------0--1-|
A--2-------|-------------2--2-------|-------------2--2-------|
E----------|------------------------|------------------------|

                                                     A major

G-------------------------|-------------------0--1-|-2--2--2--0--------0--1-|
D--2--2--2--0--------0--1-|-2--2--2--0-----2-------|-------------2--2-------|
A--------------2--2-------|-------------2----------|------------------------|
E-------------------------|------------------------|------------------------|

                            E major

G--2--2--2--0-------------|------------------------|-------------------2--3-|
D--------------2-----0--1-|-2--2--2--0--------0--1-|-2--2--2--0-----4-------|
A-----------------2-------|-------------2--2-------|-------------2----------|
E-------------------------|------------------------|------------------------|

   B major                  A major                  E major

G--4--4--4--2--------0--1-|-2--2--2--0-------------|------------------------|
D--------------4--2-------|-------------2-----0--1-|-2--2--2--0--------0--1-|
A-------------------------|----------------2-------|-------------2--2-------|
E-------------------------|------------------------|------------------------|

   B major

G-------------------------|
D--2-----------------0--1-|
A-----2--2--2--2--2-------|  repeat
E-------------------------|

This line plays the root three times, a leading note down to the fifth played
twice, and then a two-note leading sequence back to the root. It's playing
exactly the same figure under each chord: (root-root-root-lead-fifth-fifth-
lead-lead) are always played, in that order. The leading tones make it much
more driving that it would be if only roots and fifths were played: try it and
see.
   It's very common to do as this bass line does; play the same pattern under
each chord, changing the pattern up and down the fingerboard to keep the root
in the right place, but otherwise not varying the line at all. When the bass
line has this form, the pattern is often called a bass figure (or bass
pattern, or bass riff) and a lot of rock music relies heavily on such figures.
This figure is a pretty simple one: we'll run into some more simple ones later
in this lesson and into some more complex ones in later lessons.

In addition to the simple three-note chords, there are a number of four-note
chords, and also five-, six-, and seven-note chords as well. Of this vast
array of chords, only a few four-note chords are widely used outside of jazz,
and I'm only going to talk about those chords. They're made by adding one more
note onto a basic three note chord. The most commonly used four-note chord is
made by starting with a major chord and adding the note 1.5 steps above the
fifth. For example, starting with a C major chord, whose fifth is G, you would
add the note Bb, which is three half-steps above G. The following chord (which
is made of the notes C-E-G-Bb) is called a seventh chord, or a dominant chord,
and the new note is called the seventh note. You can play C7 like this:

G-----------3--
D-----2--5-----
A--3-----------
E--------------

and in general you can add the 7th note to the major scale pattern I gave
earlier, like this:

--------------------N--
-----(N-1)--(N+2)------
--N--------------------

  2   1      4      2

and get the four notes of any 7th chord you like. Seventh chords are easily
the most commonly used four-note chord. You can also make a minor seventh
chord, by starting with a minor chord instead of a major chord. For example,
the C minor 7 chord is made of the notes C, Eb, G, and Bb, and you can play
one like this:

G-----------3--
D--------5-----
A--3--6--------
E--------------

(I'll let you work out the general pattern for this one). The minor 7th chord
isn't used much in rock music (although see Gallows Pole, by Led Zeppelin, for
an interesting example of it) but it is very common in jazz music.

Another note you can add to a major chord is the note that is one whole step
above the fifth of the chord. This note is called the 6th note, and a chord
that contains it is called a 6th chord. For example, a C6 chord is made up of
the notes C, E, and G, plus the new note A (one step above G). This chord is
fingered as follows:

G-----------2--
D-----2--5-----
A--3-----------
E--------------

and it's the second most common four-note chord, after the 7th chord. The
single most widely used bass line in recorded music is based on it: if you
have ever listened to any kind of blues music, you've heard this line
somewhere. The most widely know song that uses it is probably "Johnny B.
Goode" by Chuck Berry, but there are literally thousands of songs, in all
keys, all styles and all tempos, that use it. It looks like this:

(all notes are quarter notes)

  C major 6

G-----------2-|-5--2-------|----------2-|-5--2-------|
D-----2--5----|-------5--2-|----2--5----|-------5--2-|
A--3----------|------------|-3----------|------------|
E-------------|------------|------------|------------|

  F major 6                  C major 6

G-------------|------------|----------2-|-5--2-------|
D-----------0-|-3--0-------|----2--5----|-------5--2-|
A-----0--3----|-------3--0-|-3----------|------------|
E--1----------|------------|------------|------------|

  G major 6                  C major 6

G-------------|------------|----------2-|-5--2-------|
D-----------2-|-5--2-------|----2--5----|-------5--2-|
A-----2--5----|-------5--2-|-3----------|------------|
E--3----------|------------|------------|------------|

It's based on a very simple figure: start on the root, run up the C6 chord to
the high root, then run back down again. The figure is played under three
different chords: C, F, and G, and it lasts twelve bars. The general pattern
is known as the twelve-bar blues, and it's probably the most widely used song
form in popular music. Note, for example, that Wipeout (transcribed above) is
on the same pattern, using the chords E, A, and B instead. (It uses a
different figure, but the same pattern of chords, and the same method of
repeating one figure under each chord.)

   One last point on chords in bass lines. In all of the above examples, the
first note played in each chord is the root note. Thus, we're still using the
root note to define each chord: the other notes of the chord are just helping
to flesh it out once we've already stated the main outline. Most music never
does anything else, but occasionally (most commonly in jazz) a note other than
the root will be the first (or only) note played under a given chord.
Borrowing some terms from classical music, we say that a chord is in "root
position" if the root is played first. We say that it's in "first inversion"
if the third is used to define the chord change, and in "second inversion" if
the fifth is the first note played. Second inversion is rarely used: first
inversion is usually used when playing a two-chord sequence twice in a row.
Thus, instead of playing:

   F     Bb     F     Bb

G-------------|------------|
D--------3--3-|-------3--3-|
A--1--1-------|-1--1-------|
E-------------|------------|

you might instead play:

   F     Bb     F     Bb

G-------------|------------|
D--------3--3-|-------7--7-|
A--1--1-------|-5--5-------|
E-------------|------------|

playing the chords in first inversion in the second measure, just to add
variety to the line.

For more complex chords used in jazz, you can usually play just about
any chord note you like out of them, although it's still a good idea to
start with the root note for the sake of identifying the chord. However,
for some chords, the root note doesn't sound very good under the chord;
usually this happens when another note in the chord is very dissonant
with the root. Common chords than do this include Cb5 (C flat 5) and
Cb9 (C flat 9) (or any other root note of course). In such cases you
usually do best to try first inversion, ie playing the third of the chord
on the first beat, and then moving off to either the root, or to the
dissonant note, as the case may be. I'll talk more about playing under
strange chords when I talk about scales in a later lesson.

Occasionally, a composer will specify a particular note for the bass when
writing a chord. Such chords might be referred to as "C major with an A
in the bass" which is exactly what you think - the guitar/piano plays the
C major chord but the bassist ignores that and plays the A. Chords like
that are usually written "C/A", where the letter before the slash indicates
the chord and the letter after the slash indicates the bass note. It's usually
done to give the impression that a different chord is being played. In this
example, the C major chord consists of the notes C E G ; but when the A is
added in the bass, you get the four notes A C E G which is an A minor 7th
chord. However, if the chord was written Amin7, then the guitar and piano
would probably play the A note as well, and if the composer doesn't want that
to happen for some reason, he can write "C/A" and get the desired effect.
This format can also be used to force inversions: for example, you might
see the chord "G/B" which means G major with B in the bass. This just means
that the composer wants the G chord in first inversion: you should almost
always respect the composer's wishes in such cases.

I'll end this lesson with one more (short) example of using several notes from
a chord to create a bass line. This line is based on a one-measure pattern,
and that pattern repeats, no changes, for about 5 minutes under the solos in
the middle of the song. The measure contains two chords, A minor and E7, and
each note in the line comes from one of those two chords. The song is "Light
My Fire" by the Doors, and this time I have to apologize for using a line that
was played on keyboards instead of on bass... it's too good a line to pass up!
Most bands that play this song play the line on bass anyway, so we can forgive
Mr. Manzarek some chutzpah in this case.

  A minor   E7

   q  e  e  q  e  e
G-------------------|
D--------2-----0--2-|  repeat, and repeat again!
A--0--3-----2-------|
E-------------------|

That's all there is to it, and this one measure is played for most of the
song. The first three notes are A, C, and E, the A minor chord, and the last
three notes are B, D, and E, which are the fifth, 7th, and root, respectively,
of the E7 chord.

In the next lesson I'll talk about scales, and I'll talk about what a key is,
and how the key that a song is in determines which chords are used in that 
song.

Steve

This lesson is copyright 1993 by Steve Schmidt. Permission to distribute
this lesson without charge is granted, provided that it remain unaltered,
including this notice. You may not charge money for the use of this lesson,
and you may not alter the terms of this license.
