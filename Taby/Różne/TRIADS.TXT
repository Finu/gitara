From: LIVENGOOD.MIKE@a1gw.gene.com (Mike Livengood)
Date: 29 Jun 1995 16:48:06 -0500
Subject: LESSON: TRIADS

More triads

For those who have been following the lessons that have been posted recently 
(and for those who haven't) there have been lots of references to triads (See 
Judy's excellent lesson on Chord Theory). So while we're on the subject I 
thought I'd continue the discussion with more triad usage and examples.

First let's run through a few triad shapes

Shape I				Shape II

A Major (A-C#-E)		A Minor (A-C-E)
E-----				E-----
B--5--				B--5--
G--6--				G--5--
D--7--				D--7--
A-----				A-----
E-----				E-----


Shape III			Shape IV

F Major (A-C-F)			D Major (A-D-F#)
E-----				E-----
B--6--				B--7--
G--5--				G--7--
D--7--				D--7--
A-----				A-----
E-----				E-----


Shape V				Shape VI

F# Minor (A-C#-F#)		D major (D-F#-A)
E-----				E--5--
B--7--				B--7--
G--6--				G--7--
D--7--				D-----
A-----				A-----
E-----				E-----

Shape VII			Shape VIII

D Minor (D-F-A)			G major (D-G-B)
E--5--				E--7--
B--6--				B--8--
G--7--				G--7--
D-----				D-----
A-----				A-----
E-----				E-----

Shape IX			Shape X

G Minor (D-G-A#)		A Major (C#-E-A)
E--6--				E--5--
B--8--				B--5--
G--7--				G--6--
D-----				D-----
A-----				A-----
E-----				E-----

Shape XI			Shape XII

A Major (E-A-C#)		A Minor (E-A-C)
E-----				E-----
B-----				B-----
G--6--				G--5--
D--7--				D--7--
A--7--				A--7--
E-----				E-----

These shapes represent twelve very common and very useful triads. All are 
completely movable. I just kept them in the same position for simplicity.

So what good are triads? I can see that they're just parts of chords that I 
already know...what's the big deal?

Well, let's see...one advantage of playing just the triad and not the full 6 
string chord is you can still play chords if you hurt one of your 
fingers....duh...

Another plus is simplicity of sound. Like Judy mentioned in her article, if 
two guitars are playing...full 12 string chords get pretty muddy. But if one 
guitar plays the major triad (say Shape I above) and the other guitar plays 
the complimentary major triad (Shape X or XI above) you get a cleaner more 
harmonious tone.

Another advantage is ease of transition between chords, and maintaining the 
same tones. Take a classic example of the use of triad Shapes I, III, and IV 
above. This is the intro to Amazing Journey by The Who from Tommy. The chords 
are C-G...A-E..D-A. Playing the triads makes a real nice progression.

    C   G     A   E     D   A      <Chord
E------------------------------
B---8---8-----5---5-----3---2--
G---9---7-----6---4-----2---2--
D--10---9-----7---6-----4---2--
A------------------------------
E------------------------------
   I   III    I  III    I   IV     <Shape

Also you'll find that if you play an acoustic guitar a lot that these triad 
fingerings are a lot easier on your hands than barre chords.

Here's another example that you youngsters won't recognize but is worth 
noodling with anyway. Here is intro to Squonk by Genesis off the Trick of the 
Tail CD. The use of the triads again is critical to imitate the keyboard line.

   G           D  4x  G           A
E------------------]-----------------]
B--8---8-8---8-7---]--3---3-3---3-5--]
G--7---7-7---7-7---]--4---4-4---4-6--]
D--9---9-9---9-7---]--5---5-5---5-7--]
A------------------]-----------------]
E------------------]-----------------]
  III          IV     I           I

As long as we're on the Genesis theme here is another one that uses triads. 
The tune is Everyday by Steve Hackett from the Spectral Mornings CD.

   A  B  E   A B   E Esus E  B    4x
E---------------------------------]
B--5--7--9---5-7---9-10---9--7----]
G--6--8--9---6-8---9-9----9--8----]
D--7--9--9---7-9---9-9----9--9----]
A---------------------------------]
E---------------------------------]   Bass plays E
   I  I  IV  I I     IV      I


   D  E  G   D E   G Gsus G   E   4x
E--5--7--9---5-7---9--10--9---7---]
B--7--9--10--7-9---10-10--10--9---]
G--7--9--9---7-9---9--9---9---9---]
D---------------------------------]
A---------------------------------]
E---------------------------------]   Bass plays A
  VI VI VIII  VI VI  VIII     VI

Using these triads allows chord formations around the main melody tone, which 
in this case is E-F#-G# for the first little section and A-B-C# for the second 
section, without losing the melody.


Here are a few other triad fingerings.

Shape a (IV)			Shape b (IV)

Dsus4 (A-D-G)			Dsus2 (A-D-E)
E-----				E-----
B--8--				B--5--
G--7--				G--7--
D--7--				D--7--
A-----				A-----
E-----				E-----

Shape c (VIII)			Shape d (VIII)

Gsus4 (D-G-C)			Gsus2 (D-G-A)
E--8--				E--5--
B--8--				B--8--
G--7--				G--7--
D-----				D-----
A-----				A-----
E-----				E-----

Shape e (I)			Shape f (VI)

Asus4 (A-D-E)			Dsus2 (D-E-A)
E-----				E--5--
B--5--				B--5--
G--7--				G--7--
D--7--				D-----
A-----				A-----
E-----				E-----

Take a look at the intro to Panama by Van Halen. Eddie uses the triads 
fingerings above and adds the suspended 4th triad fingering.

   E  Esus  B  E  Esus B   D  Dsus A
E-------------------------------------]
B--9--9-10--7--9---10--7---7--7-8--5--]
G--9--9-9---8--9---9---8---7--7-7--6--]
D--9--9-9---9--9---9---9---7--7-7--7--]
A-------------------------------------]
E-------------------------------------]
  IV  IV a  I  IV  a   I   IV IV a I

Notice how Shape a, the suspended 4th triad, sounds a lot like Shape III?? 
Play this:

   D  Dsus   D   G
E----------------------]
B--7---8-----7---8-----]
G--7---7-----7---7-----]
D--7---7-----7---9-----]
A----------------------]
E----------------------]

Weird huh? Maybe not...Why would a Dsus4 sound like a G? Because in 
substituting a 4th for the third in a D chord we're adding a G! So G is a 
fourth up from D. See how much we've learned?

Here's another example using yet another triad shape. This is the main theme 
to Lawyers, Guns and Money by Warren Zevon. Notice the use of the suspended 
triads.

   Asus A   Asus A     Asus A   E
E----------------------------------]
B--5--5-5---5-5-5-5----5--5-5---9--]
G--7--7-6---7-7-7-6----7--7-6---9--]
D--7--7-7---7-7-7-7----7--7-7---9--]
A----------------------------------]
E----------------------------------]
   e  e I   e e e I    e  e I  IV

Using triads in this way is the best way to cop keyboard progressions, because 
the inversions are similar to the way keyboardists play them.

Here's another example.

This is the way I play Solsbury Hill by Peter Gabriel. I use the triad shapes 
pictured above.

   F#        B-E    B    E B  F#
E--------------------------------]
B--------2---4h5-----4---5-4--2--]
G--3--4--3---4---4---4---4-4--3--]
D--4--4------4h6--4--4---6-4--4--]
A--------------------------------]
E--------------------------------]
   I        IV-III     III IV I

   F#         E        B  E  B  F#
E----------------------------------]
B--------2----5---7----4--5--4--2--]
G--3--4--3-----4---4---4--4--4--3--]
D--4--4---------6---6--4--6--4--4--]
A----------------------------------]
E----------------------------------]
   I          III      IV III IV I

Notice that a triad can be a number of different chords depending on the bass 
note? For example:

Shape III can be a major when played over an F or a minor seventh when played 
over a D.

Shape IV can be a major when played over a D or a minor seventh when played 
over a B.

Shape III			Shape IV

F Major (A-C-F)			D Major (A-D-F#)
E-----				E-----
B--6--				B--7--
G--5--				G--7--
D--7--				D--7--
A-----				A-----
E-----				E-----

Shape V can be a minor when played over an F# or a dominant seventh when 
played over a D.

Shape V

F# Minor (A-C#-F#)
E-----
B--7--
G--6--
D--7--
A-----
E-----


Here are some rather obscure triads that could have a number of names 
depending on the bass note. These triads utilize seconds, fourths and sixths, 
rather than the familiar thirds and fifths.

E-----		E-----
B--7--		B--7--
G--5--		G--9--
D--7--		D--7--
A-----		A-----
E-----		E-----

E--9--		E--7--
B--7--		B--9--
G--9--		G--7--
D-----		D-----
A-----		A-----
E-----		E-----

Some of these yield really cool progressions. Here is one that I have been 
trying to incorporate into a solo.

E--x-x-9--x-x-7--x-x-7--x-x-5--x-x-5--
B--x-x-7--x-x-7--x-x-6--x-x-5--x-x-5--
G--x-x-9--x-x-8--x-x-5--x-x-6--x-x-7--
D-------------------------------------
A-------------------------------------
E-------------------------------------

x= percussive muted strums

I guess I have rambled on enough. Once you start playing with triads instead of 
the normal fingerings for chords a whole new world of progressions, transitions 
and altered chords will open for you.

					Comments?

					M.L.



