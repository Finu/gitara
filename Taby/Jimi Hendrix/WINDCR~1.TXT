

From uunet!europa.asd.contel.com!darwin.sura.net!mips!zaphod.mps.ohio-state.edu!cis.ohio-state.edu!rutgers!cbmvax!macon Wed Jul 15 15:52:49 PDT 1992
Article: 752 of alt.guitar.tab
Path: nevada.edu!uunet!europa.asd.contel.com!darwin.sura.net!mips!zaphod.mps.ohio-state.edu!cis.ohio-state.edu!rutgers!cbmvax!macon
From: macon@gallifry.Berkeley.EDU (Glen Macon)
Newsgroups: alt.guitar.tab
Subject: music : the wind cries mary
Keywords: hendrix
Message-ID: <32978@cbmvax.commodore.com>
Date: 15 Jul 92 15:14:39 GMT
Sender: news@cbmvax.commodore.com
Reply-To: macon@cbmvax.commodore.com (Glen Macon)
Organization: COMMODORE West Cester PA
Lines: 73


                                                                       055

Title:				THE WIND CRIES MARY		(J. Hendrix)
								    1967
		                 1
		  668886 -> ->  x0888x -> ->
		|:  Eb    E  F     Eb   E  F :|

		C             Bb                 F
		After all the jacks are in their boxes
		C                   Bb          F
		And the clowns have all gone to bed
		C                      Bb            F
		You can hear happiness staggering on down the street
		G         Bb         Eb E F
		Footsteps dressed in red
		G       Bb            Eb E F  Eb E F
		And the wind whispers Mary


		C          Bb       F
		A broom is drearily sweeping
		C                       Bb          F
		Up the broken peices of yesterday's life
		C           Bb       F
		Somewhere a queen is weeping
		G           Bb          Eb E F
		Somewhere a king has no wife
		G            Bb    Eb E F  Eb E F
		And the wind cries Mary


 		LEAD  |: F  Eb  Bb  Ab :| 3x    G  Bb  Db  F


		C                       Bb     F
		The traffic lights turn blue tomorrow
		C                       Bb         F
		And shine the emptyness down on my bed
		C               Bb   F
		The tiny island sags downstream
		G                   Bb       Eb E F
		Cause the life that lived is dead
		G            Bb      Eb E F  Eb E F
		And the wind screams Mary


		C             Bb     F
		Will the wind ever remember
		C                Bb           F
		The names it has blown in the past
		C                        Bb           F
		With its crutch, its old age, and its wisdom
		G                         Bb     Eb E F
		It whispers no, this will be the last
		G            Bb    Eb E F  Eb E F  Eb E F  Eb E F
		And the wind cries Mary


\_  \_  \_  \_  \_  \_  \_  \_ 

    \_\_\_      \_      \_                  Glen Macon 
   \_            \_\_  \_\_     any-net: macon@cbmvax.commodore.com
   \_             \_ \_\_ \_            Standard disclaimer...    
    \_   \_\_\_    \_  \_  \_   Commodore doesn't endorse what I say, I do
     \_       \_    \_      \_           Who wants to know...
       \_     \_     \_      \_      
	 \_\_\_       \_      \_    
	                                                
 \_  \_  \_  \_  \_  \_  \_  \_  \_  \_

All those moments will be lost in time, like tears in the rain.


