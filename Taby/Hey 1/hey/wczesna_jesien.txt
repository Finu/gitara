

#----------------------------------PLEASE NOTE---------------------------------#
#This file is the author's own work and represents their interpretation of the #
#song. You may only use this file for private study, scholarship, or research. #
#------------------------------------------------------------------------------##
Date: 26.01.98
From: Dorota Cie�lak <pc@kki.net.pl>

                                                                           
                                 HEY                                       
                            Wczesna jesie�                                        
                          from the album '?'                             


  wst�p git.1
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[--------------2/1--------------------------------------------------------]
D[-----2-----2-------2-----------------------------------------------------]
A[-------------------------------------------------------------------------]
E[--0-----0----------------------------------------------------------------]



  git.2                                                                      
E[-------------------------------------------------------------------------]
H[--3--3/5--2/3--5--5--5---2/3--5--5--5/9--9--10--9---9/5--5--8/5--5-------]
G[-------------------------------------------------------------------------]
D[-------------------------------------------------------------------------]
A[-------------------------------------------------------------------------]
E[-------------------------------------------------------------------------]



  
E[-------------------------------------------------------------------------]
H[--7/3---5--3--2--3--2----------------------------------------------------]
G[----------------------2--2-----------------------------------------------]
D[----------------------------2--2-----------------------------------------]
A[-------------------------------------------------------------------------]
E[-------------------------------------------------------------------------]



  zwrotka git.2(a� do refrenu)
E[--0--0--0--0--0--0--0--0--7--7--7--5--5--5-------------------------------]
H[--------------------------------------------7--5-------------------------]
G[-------------------------------------------------------------------------]
D[-------------------------------------------------------------------------]
A[-------------------------------------------------------------------------]
E[-------------------------------------------------------------------------]



  ref.(x4)
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[--9--9--9--9--9--9--------------------------------------------7----------]
D[--9--9--9--9--9--9--9--7--5--7--7--7--7--7--7--7x--7x--7x--9--7----------]
A[--7--7--7--7--7--7--9--7--5--7--7--7--7--7--7--7x--7x--7x--9--5----------]
E[--------------------7--5--3--5--5--5--5--5--5--5x--5x--5x--7-------------]




  podk�ad pod solo(x12):
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[--7x--7x--9---7x--7x--9---7x--7x--9---7x--7x--7x--7x---------------------]
D[--7x--7x--9---7x--7x--9---7x--7x--9---7x--7x--7x--7x---------------------]
A[--5x--5x--7---5x--5x--7---5x--5x--7---5x--5x--5x--5x---------------------]
E[-------------------------------------------------------------------------]




