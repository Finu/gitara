

#----------------------------------PLEASE NOTE---------------------------------#
#This file is the author's own work and represents their interpretation of the #
#song. You may only use this file for private study, scholarship, or research. #
#------------------------------------------------------------------------------##
Date: 17.01.98
From: Dorota Cie�lak <pawiusz@friko.onet.pl>


                                 HEY
                                Misie
                         from the album 'Ho'

  Main riff:
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[-------------------------------------------------------------------------]
D[--5/7--7-------------5---------------------------------------------------]
A[----------5--7/5--7------------------------------------------------------]
E[-------------------------------------------------------------------------]

  riff git.1                 riff git.2(w tym samym czasie co git.1)
E[-------------------------!-----------------------------------------------]
H[--5----------------------!-----------------------------------------------]
G[--5----------------------!-----------------------------------------------]
D[--5--7--7--10--7-----7---!----7------------------------------------------]
A[--3--7--7--10--7--5--7---!----7--------------5--7------------------------]
E[-----5--5---8--5--5--5---!-8--5--5--8--5---------------------------------]
                     W pokoju obok...

  przej�cie do refrenu(zamiast riffu git.2)
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[-------------------------------------------------------------------------]
D[-----7-----------------7--5----------------------------------------------]
A[-----7-----------5--7--------7-------------------------------------------]
E[--8--5--5--8--5----------------------------------------------------------]

 ref.
    F               G              riff git.1 i 2 razem x2
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[--2--2--2--2--2---4--4--4--4--4------------------------------------------]
D[--3--3--3--3--3---5--5--5--5--5------------------------------------------]
A[--3--3--3--3--3---5--5--5--5--5------------------------------------------]
E[--1--1--1--1--1---3--3--3--3--3------------------------------------------]
