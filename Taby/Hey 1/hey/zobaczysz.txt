

#----------------------------------PLEASE NOTE---------------------------------#
#This file is the author's own work and represents their interpretation of the #
#song. You may only use this file for private study, scholarship, or research. #
#------------------------------------------------------------------------------##
Date: 13.01.98
From: Dorota Cie�lak <pawiusz@friko.onet.pl>


                                 HEY
                              Zobaczysz
                         from the album 'Fire'

    wst�p (h-e)
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[----4------4--4--4-----4------4--4--4-----4------------------------------]
D[----4------4--4--4--2--4------4--4--4--2--4------------------------------]
A[-2------2--2--2--2--2--2---2--2--2--2--2--2---2--0--0--------------------]
E[-------------------------------------------------------------------------]



     main riff (h-e)
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[---4--4--4-----4---------------------------------------------------------]
D[---4--4--4--2--4--0--0--4--5--5--4--0------------------------------------]
A[---2--2--2--2--2---------------------------------------------------------]
E[-------------------------------------------------------------------------]

    przed pauz� (h-e)
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[--4--4--4-----4----------------------------------------------------------]
D[--4--4--4--2--4----------------------------------------------------------]
A[--2--2--2--2--2----------------------------------------------------------]
E[-------------------------------------------------------------------------]


  Gdy wchodzi wokal - main riffx4 :   (G-Fis)
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[-------------------------------------------------------------------------]
D[--5--4-------------------------------------------------------------------]
A[--5--4-------------------------------------------------------------------]
E[--3--2-------------------------------------------------------------------]


  Podczas refrenu ma�o s�ycha� gitar�, ale brzmi to jako� tak: (h-g-b-h-fis-h-g)
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[--4-----3--4-----4-------------------------------------------------------]
D[--4--5--3--4--4--4--5----------------------------------------------------]
A[--2--5--1--2--4--2--5--2--1--2-------------------------------------------]
E[-----3--------2-----3----------------------------------------------------]


  zako�czenie (h-e)						(g-e)
E[-------------------------------------------------------------------------]
H[-------------------------------------------------------------------------]
G[--4--4-----4--4-----4--4-----4--4-----4--4-----4--4------4---------------]
D[--4--4--2--4--4--2--4--4--2--4--4--2--4--4--2--4--4--2---2--5--4---------]
A[--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2--2---2--5--4---------]
E[------------------------------------------------------------3--2---------]

