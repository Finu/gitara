             	         Hope Of Deliverance
                           Paul McCartney

Capo 5

 Cadd9 (x32030)



 Intro G Em G Em


               G  Em      Am                      G  Em       Am

 I will always be hoping, hoping. You will always be holding, holding

                                    G    Em G Em

       my heart in your hand. I will understand.


              G    Em       Am                      G    Em

 I will understand someday, one day. You will understand always,

Am

always from now until then.


                 Em       Am                          Em      Am

 When it will be right, I don't know. What it will be like, I don't

                 C          Em/B              Am               C/G

know. We live in hope of deliverance from the darkness that surrounds


us.


 C          Em/B      C    Cadd9 Em/B      C          Em/B

 Hope of deliverance, hope of deliverance. Hope of deliverance from

    Am               C/G      G Em G Em

        the darkness that surrounds us.


                G    Em       Am                        G    Em
 And I wouldn't mind knowing, knowing that you wouldn't mind going,
Am
going along with my plan.

                 Em       Am                          Em      Am
 When it will be right, I don't know. What it will be like, I don't
                 C          Em/B              Am               C/G
know. We live in hope of deliverance from the darkness that surrounds

us.

 C          Em/B      C    Cadd9 Em/B      C          Em/B
 Hope of deliverance, hope of deliverance. Hope of deliverance from
    Am               C/G      G Em G Em
the darkness that surrounds us.

Solo G Em Am G Em Am Em Am Em Am C Em/B Am C/G

 C          Em/B      C    Cadd9 Em/B      C          Em/B
 Hope of deliverance, hope of deliverance. Hope of deliverance from
    Am               C/G      G Em G Em
the darkness that surrounds us.

 C          Em/B      C    Cadd9 Em/B      C          Em/B
 Hope of deliverance, hope of deliverance. Hope of deliverance from
    Am               C/G      G Em G Em
the darkness that surrounds us.

G Em G Em

G          Em
Hope of deliverance. etc...


