Name STILL GOT THE BLUES
Interpret Gary Moore
Jahr 1990
Schreib�r Moore

Used to be so easy
to give my heart away
But I've found that the hard way
is a price you have to pay
I found that love
was no friend of mine
I should have known
time after time

So long, it was so long ago
but I still got the blues for you

Used to be so easy
to fall in love again
I've found that the hard way
it's a way that leads to pain
I've found that love
was more than just a game
to play and to win
but to lose just the same

so many years
since I've seen your face
and now in my heart
there's an empty space
where you used to be

Though the days come and go
there's one thing I know
I still got the blues for you

 

