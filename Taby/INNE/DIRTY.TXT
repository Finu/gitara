She's Like The Wind
	Patrick Swayze
	"Dirty Dancing" Soundtrack


She's like the wind through my tree
She rides the night next to me
She leads me through moonlight
Only to burn me with the sun
She's taken my heart
But she deosn't know what she's done

CHORUS:
Feel her breath on my face
Her body close to me
Can't look in her eyes
She's out of my league
Just a fool to believe
I have anything she needs
She's like the wind

I look in the mirror and all I see
Is a young old man with only a dream
Am I just fooling myself
That she'll stop the pain
Living without her
I'd go insane

CHORUS


 

