#----------------------------------PLEASE NOTE---------------------------------#
#This file is the author's own work and represents their interpretation of the #
#song. You may only use this file for private study, scholarship, or research. #
#------------------------------------------------------------------------------##
Title: Hunting High And Low
Artist: A-Ha

[Am]Here I am and with[F]in the reach of
my [Fm]hand she [Am]sound asleep and she's
[F]sweeter now than my [Db]wildest dream
[Cm]could have seen her. And I wa[Ab]tch her
Slipping a[G]way. But I [F]know I'll be
[G]hunting [Asus4]high and low [G] [Fmaj7]high__[Dm]
There's no [Am7]end to the lengths [A7]I'll [Dm]go [Em]to,
[F]hunt[G]ing [Asus4]high and low [G] [Fmaj7]high [Dm]
There's no [C] end to the [Em7] lenghts I'll [Dm]go [G]to.

[Am]Find her again, upon [F]this my dreams are [Fm]depending.
[Am]Through the dark, I [F]sense pounding of [Db]her heart [Cm]next
to mine. She's the [Ab]sweetest love I could [G]find. So [F]I'll guess
I'll be [G]hunting [Asus4]high and low, [G] [Fmaj7]high__[Dm]. There's no
[Am7]end to the lenghts [A7]I'll [Dm]go [Em]to, [Asus4]high and low,
[G] [Fmaj7]high__[Dm]. Do you [C]know what it [Em7]means to [Dm]love [G]you.

[Am]  [G] [F] [Dm7] [C]   [B]     [Am]     [Em]    [F]

[G]  I'm hunting [Am] high and low [G] and [Fmaj7] now she's telling
me she's got to [Am]go away

[G] [F]  [Dm]

  [Cm]           [Bb]             [Ab]             [G]
e---3-1-3-4-3-1-3----3-1-3-4-3-1-3----3-1-3-4-3-1-3-3~~~~~~-----------
H-1-------------------------------------------------------------------
G------------------3----------------1---------------------------------
D---------------------------------------------------------------------
A---------------------------------------------------------------------
E---------------------------------------------------------------------

[F]I'll always be hu[G]nting [Asus4]high and low[G] [Fmaj7]only for you
[Dm] Watch me [Am7]tearing myself [A7]to [Dm]piec[Em]es, [F]hun[G]ting
[Asus4]high and low [G] [Fmaj7]high_____[Dm] There's no [C]end to the
[Em7]lenghts I'll [Dm7]go to [Em7]  [F] for y[G]ou I'll be hunting
[Am]high and low [G] [F] [C] [Dm] [A]

Far to be exact, but I hope someone will correct this to it's perfection :)
E-mail me corrections please ! (and any other songs if you have them, or at
least pieces of 'em). Sure, you can also flame me about this !


                                                     beno.janzek@uni-mb.si
