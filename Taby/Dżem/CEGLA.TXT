DZEM: Cegla (1985)
"Czerwony jak cegla"

I solo (Jurek Styczynski)

    E            A         E                H             E     A
E-|---------------------------------------------------------------------------|
B-|------------------------------12-------------------------------------------|
G-|---------9----11b12p9--11b13---------------------9-9-11b12---12----9h11---9|
D-|-9-10-11---11--------------------11b12u11-9--/11----------------11------11-|
A-|---------------------------------------------------------------------------|
E-|---------------------------------------------------------------------------|
                                                                               
    E                       H            E       A                 E
E-|-----------------------------------12-12~-12~-----12-12-12~~-12-14b16-14b16|
B-|-12b14-12b14-12b14-12b14-12--12-14------------/14--------------------------|
G-|---------------------------------------------------------------------------|
D-|---------------------------------------------------------------------------|
A-|---------------------------------------------------------------------------|
E-|---------------------------------------------------------------------------|
                                                                               
                H             E           fis             A
E-|-14b16-14b15-14--------12-14b16--------14b15-12--------15b17u16b17u16b17u16|
B-|-----------------12-14-----------12-14-----------/14-17--------------------|
G-|---------------------------------------------------------------------------|
D-|---------------------------------------------------------------------------|
A-|---------------------------------------------------------------------------|
E-|---------------------------------------------------------------------------|
                                                                               
           E                  H
E-|-17b16u-17b19u18b19-15-----------------------------------------------------|
B-|-----------------------17--/12-15-12-15-12---------------------------------|
G-|-------------------------------------------14-12~--------------------------|
D-|--------------------------------------------------14\----------------------|
A-|---------------------------------------------------------------------------|
E-|---------------------------------------------------------------------------|
                                                                              
spisano (wg wersji z plyty "Cegla") przez:

Bart
<bm@venus.wmid.amu.edu.pl>
<rzaba@kki.net.pl>
<bartmal@polbox.com>
http://free.polbox.pl/b/bartmal

