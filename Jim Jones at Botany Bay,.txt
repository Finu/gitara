-------------------------------------------------------------------------------
            Jim Jones at Botany Bay -- "The Hateful Eight" version
-------------------------------------------------------------------------------
Tabbed by: FeynmansZombie

Tuning: half-step down

This is the version of the song played by Daisy (Jennifer Jason Leigh) in Tarantino's movie "The Hateful Eight."
Probably the best scene of the movie. As far as I can tell, it's an original arrangement - I tabbed it by ear
by listening to the soundtrack. This version is more similar to "Skibbereen" than it is to other versions of 
"Jim Jones at Botany Bay." The lyrics are also different in some places (especially the last line).

It's all four chords - stick to them and you can make up your own flourishes if you like. I won't pretend to 
know every note and nuance, but this was basically a live performance so it wouldn't do to be too scripted. For
instance, you could play G with the B string open or 3, or switch it up as you please.

Intro (also a motif that appears in every line):

eb|--------------------------------------------|
Bb|-------0------------------0-----------------|
Gb|----------------0-------------------0-------|
Db|--------------------------------------------|
Ab|-----------0h2-----------------0h2----------|
Eb|--0------------------0----------------------|



1st Verse:


      G                                    Am                Em
eb|---------------------------------------------------------------------------|
Bb|--------3-----------------3------------------1-----------------0-----------|
Gb|-----------------0-----------------0------------------2------------------0-|
Db|---------------------------------------------------------------------------|
Ab|------------------------------2---------0---------------------------0h2----|
Eb|---3--------3--------3---------------------------0--------0----------------|

      Listen   for a    moment   lads, and hear me  tell my  tale,



     C                 G                 Am                Em
eb|---------------------------------------------------------------------------|
Bb|-------1-----------------3-----------------1-----------------0-------------|
Gb|----------------0-----------------0-----------------2-----------------0----|
Db|---------------------------------------------------------------------------|
Ab|--3--------3-----------------2--------0--------------------------0h2-------|
Eb|--------------------3--------------------------0--------0------------------|

 How o'er the sea from England's shore I was con- demned to sail.


                                                       Notice the extra two beats ------
                                                       at the end of some lines.        |
                                                                                        |
                                                                                        |
                                                                                        V
     C                 G                 Am                Em
eb|--------------------------------------------------------------------------------------------|
Bb|-------1-----------------3-----------------1-----------------0------------------0-----------|
Gb|----------------0-----------------0-----------------2-----------------0-------------------0-|
Db|--------------------------------------------------------------------------------------------|
Ab|--3--------3-----------------2--------0--------------------------0h2---------------0h2------|
Eb|--------------------3---------------------------0-------0-----------------0-----------------|

 The jury     found me "guilty  sir" and said the judge said he,


     Am                Em                 Am                Em
eb|-------------------------------------------------------------------------------------------|
Bb|------1------------------0-----------------1-----------------0-----------------0-----------|
Gb|----------------2----------------0------------------2-----------------0-----------------0--|
Db|-------------------------------------------------------------------------------------------|
Ab|--0--------0--------------------------0--------------------------0h2---------------0h2-----|
Eb|--------------------0--------0-----------------0--------0-----------------0----------------|

For life, Jim Jones, I sentence you  a-  cross the stormy  sea.


Other verses proceed the same, always with the extra two beats at the end of the 3rd and 4th lines:


G                              Am            Em
Take my tip before you ship to join the iron gang,

      C             G              Am                 Em
Don't be too gay in Botany Bay, or else you'll surely hang.

   C                  G                  Am              Em
Or else you'll surely hang, said he, and after that, Jim Jones,

     Am          Em                 Am                   Em
High up upon the gallows tree, the crows will pick your bones.

There's no chance for mischief there, remember that, they say
They'll flog the poaching out of you down there in Botany Bay.
Waves were high upon the sea, the winds approaching gales
I'd rather drowned in misery than gone to New South Wales.

The waves were high upon the seas when pirates came along,
But the soldiers on our convict ship were full five hundred strong.
They opened fire and somehow drove that pirate ship away
I'd rather joined that pirate ship than gone to Botany Bay.

And one dark night when everything is quiet in the town,
I'll kill you bastards one and all, I'll gun the floggers down.
I'll give them all a little shock, remember what I say
They'll yet regret they sent Jim Jones in chains to Botany Bay.

Now day and night in irons clad we like poor galley slaves
Toil and toil and when we die must fill dishonored graves
By and by I'll break m' chains and to the bush I'll go
And you'll be dead behind me, John, when I get to Mexico.

